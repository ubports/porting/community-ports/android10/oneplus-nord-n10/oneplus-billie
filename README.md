# OnePlus Nord N10 5G
______________________

[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

![](./refs/oneplusnordn10.png)
______________________
OnePlus Nord N10 5G


OnePlus Nord N10 5G (billie) specs
==========================================


| Basic                   | Spec Sheet                                                                                                                     |
| -----------------------:|:------------------------------------------------------------------------------------------------------------------------------ |
| CPU                     | Octa-core (2x2.0 GHz Kryo 560 Gold & 6x1.7 GHz Kryo 560 Silver) Silver)                                                                                                                      |
| Chipset                 | Qualcomm SM6350 Snapdragon 690 5G (8 nm)                                                                                                            |
| GPU                     | Adreno 619L                                                                                                                   |
| Memory                  | 6 GB RAM                                                                                                                     |
| Shipped Android Version | Android 10, OxygenOS 10.50                                                                                                                            |
| Storage                 |128GB                                                                                                                  |
| Battery                 | Li-Po 4300 mAh, non-removable battery                                                                                           |
| Display                 | 1080 x 2400 pixels, 20:9 ratio (~406 ppi density)                                                                            |
| Camera (Back)(Main)     | 64 MP, f/1.8, (wide), 1/1.72", 0.8µm, PDAF,8 MP, f/2.3, 119˚ (ultrawide),2 MP, f/2.4, (depth),2 MP, f/2.4, (macro), OIS                                                                                |
| Camera (Front)          | 16 MP, f/2.1

## Requirements
- The procedure requires a computer running Win10 or Win11. Using a virtualized Windows is impossible.

## Firmware
- This builds are based on OxygenOS 10.5.7 for UE and 10.5.7 for Global
- To flash the device is required that device is in EDL mode please check https://community.oneplus.com/thread/1541327

| Region                   | firmware                                                                                                                     |
| -----------------------:|:------------------------------------------------------------------------------------------------------------------------------ |
|EU | BE89BA https://onepluscommunityserver.com/list/Unbrick_Tools/OnePlus_Nord_N10_5G/EU_BE89BA/Q/OnePlus_Nord_N10_5G_EU_OxygenOS_10.5.7.zip
|Global | BE86AA https://onepluscommunityserver.com/list/Unbrick_Tools/OnePlus_Nord_N10_5G/Global_BE86AA/Q/OnePlus_Nord_N10_5G_Global_OxygenOS_10.5.7.zip


## Unsuported Devices/versions/Firmware

 Region                   | firmware                                                                                                                     |
| -----------------------:|:------------------------------------------------------------------------------------------------------------------------------ |
|US Metro by T-Mobile| BE88CF https://onepluscommunityserver.com/list/Unbrick_Tools/OnePlus_Nord_N100/Metro_by_T-Mobile_BE82CF/Q/OnePlus_Nord_N100_Metro-by-T-Mobile_OxygenOS_10.5.8.zip
|US T-Mobile | BE88CB https://onepluscommunityserver.com/list/Unbrick_Tools/OnePlus_Nord_N10_5G/T-Mobile_BE88CB/Q/OnePlus_Nord_N10_5G_T-Mobile_OxygenOS_10.5.8.zip

## Verizon variants dont even try the bootloader is locked and cant be unlocked.

## 

- Install Qualcomm USB drivers v1.0 on a Win10/Win11 machine
- Plug the phone running EDL mode
- Go on the devices manager and select the QL Loader 9008 driver, then restart windows
- Run the MSM tool and enum the device. If the COM number doesn't match with the device manager and MSM tool, restart Windows and try again.
- After enumerating the phone with the MSM tool, start the flashing procedure

## Unlock bootloader
- Boot the device, do not connect to wifi or create any account we don not need that. Setup the device offline
- Once you boot to the menu go to Settings -> About phone and tap build number until you see " You are now a Developer"

</div>
- Now Connect to wifi network
- Go back to Settings -> System -> Developer Options and enable OEM Unlocking.
- Power off the device VOLUME UP + POWER
- Power on Holding VOLUME UP + POWER to enter FASTBOOT MODE
- Now run the following command:

```console
fastboot flashing unlock
```
- Confirm on the device using the VOLUME keys and the power button to unlock the bootloader.

### Install from CI

- Download latest CI build
- Flash boot.img:
```console
fastboot flash boot ./boot.img
```
- Flash recovery.img:
```console
fastboot flash recovery ./recovery.img
```
- Reboot to fastbootd:
```console
fastboot reboot fastboot
```
- Format userdata
```console
fastboot format:ext4 userdata
```
- Delete logic partitions:
```console
fastboot delete-logical-partition system_ext_a
fastboot delete-logical-partition system_ext_b
fastboot delete-logical-partition product_a
fastboot delete-logical-partition product_b
fastboot delete-logical-partition system_b
fastboot delete-logical-partition vendor_b
fastboot delete-logical-partition odm_b
```
- Resize system_a partition
```console
fastboot resize-logical-partition system_a 3221225472
```
- Flash ubuntu Rootfs
```console
fastboot flash system_a ./ubuntu.img
```
- Reboot the device
```console
fastboot reboot
```
# What works so far?

### Progress
![100%](https://progress-bar.dev/100) Ubuntu 20.04 Focal

- [X] Recovery
- [X] Boot
- [X] Bluetooth
- [X] Camera Fotos and Video Recording
- [X] GPS
- [X] NFC
- [X] Audio works
- [X] Bluetooth Audio
- [X] Waydroid
- [X] MTP
- [X] ADB
- [X] SSH
- [X] Online charge
- [X] Offline Charge
- [X] Wifi
- [X] Calls
- [X] Mobile Data 2G/3G/4G (LTE)
- [X] Ofono
- [X] SDCard
- [X] Wireless display
- [X] Fingerprint Reader
- [x] OTG Works
- [X] Camera Flash
- [X] Manual Brightness Works
- [X] Switching between cameras
- [X] Hardware video playback
- [X] Rotation
- [X] Proximity sensor
- [X] Virtualization
- [X] GPU
- [X] Lightsensor
- [X] Proximity sensor
- [X] Automatic brightness
- [X] Torch
- [X] Hotspot
- [X] Airplane Mode

## Im not endorsed or founded by UBports Foundation and i do this for fun but does take time work and resources, so if you want to support and apreciate my work pleaso do Donate:
___________________________________
<center>
<a href="https://paypal.me/rubencarneiro?locale.x=pt_PT" title="PayPal" onclick="javascript:window.open('https://paypal.me/rubencarneiro?locale.x=pt_PT','WIPaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1060, height=700'); return false;"><img src="https://www.paypalobjects.com/webstatic/mktg/Logo/pp-logo-150px.png" border="0" alt="PayPal Logo"></a>